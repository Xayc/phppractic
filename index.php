<html>
    <head>
        <meta charset="utf-8" />
    </head>

    <body>
        <h1>Задания на практические работы по PHP</h1>
        <ol>
            <li><a href="/simple_arithmetic/">Простая арифметика</a></li>
            <li><a href="/conditions/">Условия</a></li>
            <li><a href="/cycles/">Циклы</a></li>
            <li><a href="/strings/">Строки</a></li>
            <li><a href="/functions/">Функции</a></li>
        </ol>
    </body>
</html>